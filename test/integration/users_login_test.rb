require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "login with invalid information" do
    get login_path # Visit the login path.
    assert_template 'sessions/new' # Verify if new form renders properly
    post login_path, session: { email: "", password: ""}
    assert_template 'sessions/new'
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end

  test "login with valid information followed by logout" do
    get login_path  #Entra na pagina
    # Preenche os dados da pagina
    post login_path, session: { email: @user.email, password: 'password' }
    # Testa se o usuario esta logado
    assert is_logged_in?
    # Testa se foi redirecionado para a pagina do usuario
    assert_redirected_to @user
    follow_redirect! # Visita realmente a pagina do usuario
    assert_template 'users/show' # Testa se o template usado é o 'users/show'
    # Testa se possui o link do login_path, count 0 significa que possui 0
    # links correspondente a login_path
    assert_select "a[href=?]", login_path, count: 0
    # Testa se possui o link de logout na pagina de usuario logado
    assert_select "a[href=?]", logout_path
    # Testa se o link do profile do usuario aparece
    assert_select "a[href=?]", user_path(@user)
    # Desloga o usuario
    delete logout_path
    # Testa se o usuario nao esta logado
    assert_not is_logged_in?
    # Testa se o usuario foi redirecionado para a url root
    assert_redirected_to root_url
    # Simula o usuario clicando em logou em uma segunda janela
    delete logout_path
    # Realmente redireciona para url root
    follow_redirect!
    # Testa se possui o link de login no root
    assert_select "a[href=?]", login_path
    # Testa se NAO possui o logout link no root
    assert_select "a[href=?]", logout_path, count: 0
    # Testa se NAO possui o link de profile do usuario no root
    assert_select "a[href=?]", user_path(@user), count: 0
  end

  test "login with remembering" do
    log_in_as(@user, remember_me: '1')
    assert_not_nil cookies['remember_token']
  end

  test "login without remembering" do
    log_in_as(@user, remember_me: '0')
    assert_nil cookies['remember_token']
  end
end
