require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
  end

  test "unsuccessful edit" do
    # Efetura o login do usuario
    log_in_as(@user)
    # Pega o caminho para users/edit do usuario
    get edit_user_path(@user)
    # Testa se o template eh users/edit
    assert_template 'users/edit'
    # Preenche e envia dados inválidos no formulário
    patch user_path(@user), user: {
      name: "",
      email: "foo@invalid",
      password: "foo",
      password_confirmation: "bar"
    }
    # Testa se o template continua como users/edit
    assert_template 'users/edit'
  end

  test "successful edit with friendly forwarding" do
    get edit_user_path(@user)
    # Usuario efetua o login
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    name = "Foo Bar"
    email = "foo@bar.com"
    patch user_path(@user), user: {
      name: name,
      email: email,
      password: "",
      password_confirmation: ""
    }
    # Testa se a barra de mensagem de erro não está vazia
    assert_not flash.empty?
    # Testa se foi redirecionado para a tela do usuário
    assert_redirected_to  @user
    # Realmente visita pagina do usuario
    @user.reload
    # Testa se o nome do usuário e email foram atualizados
    assert_equal name, @user.name
    assert_equal email, @user.email
  end
end
